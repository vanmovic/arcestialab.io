---
layout: keynote
title: What is i do on internet?.
subtitle: What Website i often visit? wanna know? let's see.
iframe: "https://www.youtube.com/embed/oRVv1DuAgH8?rel=0&amp;controls=0&amp;showinfo=0&amp;autoplay=1"
date:       2016-09-26
author:     "Arcestia"
tags:
- story
- myself
- blog
- life
- my life
- curhat
---

Hey Guys, ini dia salah satu post yang dari kemarin gw udah post di facebook dan akhirnya baru bisa gw ketik diblog deh.
for info untuk kalian sebelumnya yuk kalian ikutan share apasih yang sebenarnya kalian lakukan di internet? apakah sama seperti saya?

okay enough buat intermezzoNya sekarang back to topic ya,

### What i do on Internet?

**This is the answer,**

- Social Media ( siapa sih yang gak punya sosmed? )
- Reading (gw sering banget baca2 post di ScienceAlert, TheHackerNews, Medium, TechCrunch, TechinAsia dan lainnya deh)
- Streaming (Youtube, Spotify, and Netflix for sometime or with Popcorn Time if my Netflix Acc Expired)
- Study (nah ini yang bakal gw bahas di section berikutnya)

### How i study on Internet?

Okay Guys, banyak banget yang pengen tahu gimana sih gw belajar di Internet. kok bisa gw tahu banyak hal?
hal ini juga muncul dari pertanyaan cewek gw yang juga pengen tahu ternyata.

Sebenarnya gampang kok caranya kalau mau banyak tahu, cukup banyak2 baca aja.
yang bikin gw ngerti adalah karena gw sering banget nemu konflik masalah dan langsung gw search deh penyebabnya dan solusinya.
makanya dari internet gw belajar tentang psikologi, tentang banyak hal diluar kompetensi gw sendiri.
Dan kebetulan gw belajar banyak bahasa asing juga, salah satunya sekarang masih mendalami vietnam nih.

**Situs apa aja sih yang sering jadi media belajar gw?**
*kebanyakan gw udah sebut diatas sih cuma bikin listnya deh*

- TechinAsia (techinasia.com)
- TechCrunch (techcrunch.com)
- TheHackerNews (thehackernews.com)
- Medium (medium.com)
- ScienceAlert (sciencealert.com)
- Curiosity (curiosity.com)
- Reddit (Reddit.com)
- PCMag (pcmag.com)
- The Verge (theverge.com)
- Engadget (engadget.com)
- Wired (wired.com)
- TED (ted.com)
- Vimeo (vimeo.com)
- Github (github.com)
- Community (Slack, IRC, and etc)
- dan lainnya dari Webminar (Web Seminar)

okay guys kira2 itu yang gw lakuin saat berinternet, ayo apa yang kalian lakukan di internet?
Comment dibawah ya guys pendapat kalian.

Arcestia, <br>
September 26, 2016
