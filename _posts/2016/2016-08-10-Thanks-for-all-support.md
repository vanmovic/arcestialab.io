---
layout: post
title: Thanks for all support!!
subtitle:   "Sorry if I can't reply all your message to me, but thanks guys."
date:       2016-08-10 12:00:00
author:     "Arcestia"
header-img: "img/post-bg-thanksfull.jpg"
catalog: true
tags:
- story
- my life
- Thanks
---
# Intro

In this post i have to tell you few thing, but before that this is my new me with my blog.
Okay this will a bit long post so take your time guys.

# First

Let me tell you from now I be an Independent Developer? yes I have been out from my team in Skymedia and held all my responsibilities to other hand.
but I'll keep updated to it, all report and services under my control will held in by new people. Okay that was sad but time was pass and we have to go.

# Second

For now I have some vision for what will I do from now.

## Website

- I Have recoding all my website from dynamic site to static site using Jekyll and Github Pages.
- From now I using my own Server to Cache Server (Backup Server) that providing stabilities to all my site.
- Creating Jekyll API for all my static site that can used to Mobile Apps or other kind. (On Progress)

## Work

- Focus on my Freelance Project.
- Back to Fullstack Web Developer, no more in just with backend.
- Comeback Active selling my work in Envato Market, some people want me update my themes at there and i will.
- Try to Working in team again, I hope i can but should try.

## Social

- Will have in College, oh god i love this one.
- Having someone is Good, but having intent relationship is under my control.
- Try LDR again? sorry that's enough for me, alone is better from now.
- Friends? of course i will happy meet new friends.

## Studies

- Finish College
- Post Graduated? of course i never stop before i have Phd named in my name.
- Still taking Computer science or other? may be i take other major.
- Will Taking English literature or Psychology depend my brain.

## Dream

- Finish my Education
- Find friends in same vision and make new Startup.
- Hope that's my startup can raise $1B in single round and be Startup Dragon, Yes Dragon cause i hate Unicorn.
- Have someone that care about me, someone special that can take my time from this monitor.

# Third

Time have been running out, and i have to take risk to reach my vision above. but for sure I have to hold my desire.
and finally decision have been made, Thanks for read this message hope all of you take risk to reach your own dream.
Thanks for your support guys, i'll keep for all of you.

Deeply wishes, <br>
Arcestia
