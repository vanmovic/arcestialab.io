---
layout: post
title: How Stupid I'am with my Girlfriend
subtitle:   When sometime i do stupid things when chat or call with my Girlfriend.
date:       2016-10-02
author:     "Arcestia"
header-img: "img/post-bg-fool.jpg"
tags:
- curhat
- myself
- my life
---
# Hi Guys

Okay pertama sebenarnya post ini harusnya muncul di akhir bulan September lalu, tapi berhubung di tawarin project music sama temen jadi post ini gak kelar2 deh. sebelumnya jangan lupa cek soundcloud gw di https://soundcloud.com/arcestiaishere/ dan selain itu emang gw lagi sibuk dengan tugas yang akhir2 ini lumayan menumpuk.

# Back to Topic

Okay guys, gw mau cerita nih tentang tingkah laku kebodohan gw saat bareng pacar. Biasa sih kalau cowo suka jailin pacarnya ya kan hahaha, tapi beda loh guys kalau hubungan gw justru sering banget gw dikerjain sama cewe gw guys. Kasian banget ya hidup gw guys, tetapi semua itu gw bales dan tingkah laku gw yang bodoh dan pura2 gak tahu tentang apa yang dia omongin. Ya kadang gw sering banget gak jelas ngebahas topik yang gak nyambung, karena gw males juga kalau pacaran terlalu dibawah serius guys. Jadi kadang emang gw banyak bercanda sih kalau lagi call cewe gw, dan terkadang gw juga bingung sih sama candaan gw sendiri.

# Act as a fool

Bertingkah sebagai seorang bodoh kadang itu perlu, dalam kartu tarot "The Fool" atau "Si Bodoh" adalah kartu paling hebat bukan karena yang buat kartu tarot bodoh ya, tetapi karena kebodohannya seseorang tidak akan dicurigai. itu yang membuat seseorang yang bertingkah bodoh justru dia bisa leluasa untuk bertindak.

Tapi sebenarnya bukan itu sih tujuannya gw bertindak bodoh ya, gw bertindak bodoh karena gw mau menjadi orang bodoh yang akan tetap setia dengan dia yang jauh disana. gw bodoh karena gw gak bisa melihat sekeliling gw yang justru lebih baik dari dia, gw terus mencoba untuk tetap setia walaupun jarak dan waktu memisahkan kita. gw juga terus mencoba berkomunikasi sesering mungkin dan terus perhatian walaupun kadang gw lebih asik dengan apa yang gw kerjakan tetapi selalu ada waktu yang diluangkan untuknya, karena gw bodoh gw tetap bertahan dengan hubungan ini. mungkin kebanyakan orang tidak sukses menjalani Long Distance Relationship (LDR) atau hubungan jarak jauh, ya karena mereka muncul ketidakpercayaan pada pasangannya yang jauh disana. ya karena kebodohan gw justru percaya dengan pasangan gw disana, walaupun gw gak pernah tahu apa yang dilakukan disana dan sama siapa aja dia ketemu dan ngapain aja dia.

# Stupid cause i trust and believe

beberapa teman bilang gw bodoh karena gw tetap percaya dan yakin pada hubungan jarak jauh yang gw jalanin. Ya kadang gw berpikir dan mulai sempat curiga dengan pasangan gw sih, cuma gw tutup rapat semua prasangkah itu dan terus percaya dan yakin padanya. pada intinya gw tipe orang yang cenderung overprotektif dan cemburuan, jadi kalau cewe gw nelpon gw selalu tanya dengan pertanyaan yang simple "Kamu hari ini ketemu siapa aja?" pertanyaan yang simple, bersikap protektif dan cemburu. tapi semua itu wajar, karena gw juga kasih tahu apa yang gw lakukan dan bahkan gw pernah bilang gini guys. **Hari ini aku abis ketemu banyak teman2 baru loh, tapi dari sekian banyak teman baru yang aku temui mataku tertuju pada 1 orang yang duduk menyendiri dipojokan. aku seperti melihat seseorang yang familiar, perasaan ini timbul karena aku yakin bahwa dia pernah dekat denganku. Ya dia seseorang perempuan dengan sikap malu dan pendiam, hati ini gak bisa bohong kalau muncul sebuah perasaan. Aku tahu kalau seharusnya perasaan ini tidak muncul, aku mencoba untuk lari dari semua itu tetapi aku butuh bantuan untuk melalui itu semua, semoga kamu mau membantuku melaluinya.** kira gitu deh guys ceritanya.

Okay deh guys sekian post gw kali ini ya. <br>
See ya di post gw selanjutnya.

Arcestia, <br>
October 2nd, 2016
